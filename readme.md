Green crab processing
----
# Haida Gwaii
`1a_egc_haidagwaii_processing.R`
- Process Haida Gwaii data from CHN (Stu Crawford sent 2022-03-15)
- includes headers and trap-level data. Crab-level data is in sheet from Stu but not incorporated into this yet. TBD.

## Data cleaning:
- removed "was this trap functional" = N
- calculated duration as difference in start and end date
- add PFMAs, using lookup table from spatial join in ArcMap
- add AIS beach names to sites (geogLoc) where clear/possible, otherwise assign new name from geographic features

## HG to do:
- add crab-level data (sex, size)
- add new beach names to site name database

# DFO core program
`1b_egc_core_processing.R`
- Process FFHPP data from DFO core program (Patty Menning sent 2021-12-15)
- includes headers, trap-level data, and crab-level data. 

## Data cleaning:
- corrected a few presumed typos re: year
- duration calculated as difference in start and end time. Used provided "soak time" field only when missing start and/or end time (i.e., when calculated duration is NA)
- add a sequential number to make set numbers unique - joining the catch and headers is a bit messy, recommend more standard setID
- replaced non-standard species names with codes

## Core to do:
- compare/standardize core site names with our site name database
- check for out-of-range soak times (currently set >1000 h soak to NA, removing from dataset)

# DFO Science
`1_egc_processing.R`:
process the following data sources: 
- DFO science crab data from crabBio.   
- missing DFO science data data (currently, April 2021 central coast data not in crabBio)
- DFO core data (processed in `1b_egc_core_processing.R`)
- Haida Gwaii data (processed in `1a_egc_haidagwaii_processing.R`)

replace site names (geogLoc) with names from site-renaming project. 
-- These site names are standardized to beaches, and will replace the poorly-maintained geogLoc in the database currently.
- set a unique set ID by concatenating data source, date, set number, and site name. Site name is needed because set number is not always unique per day.

data cleaning:
-- remove trap codes 1, 2, 3, and 18
-- remove "GD" source (depletion trapping)
-- keep only Fukui traps
-- remove sets with zero duration

set-level data
- CPUE calculated as nCrabs per trap*hour
- includes one line for each species (including zeros), with columns for each sex, for each set

site-level data
- calculates CPUE as sum(nCrabs)/(sum(nTraps)*sum(duration)) per month per site,
- coordinates are the average of all coordinates for each named site. 

Outputs = 
- all crabs by set (shapefile and csv)
- EGC by set (csv and shapefile, just a subset of above)
- all crabs by site (shapefile and csv)
- EGC by site (csv and shapefile, just a subset of above)

## DFO SCI To-do
- 37 "possible GPS errors" that do not have site names currently
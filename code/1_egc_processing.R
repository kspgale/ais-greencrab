# Process European Green Crab data from raw catch
## Katie Gale/Brett Howard - DFO

# SQL code for initial query
# SELECT Headers.Key, Headers.Source, Headers.Year, Headers.Month, Headers.Day, Headers.GeogLoc, Headers.MinDepth, Headers.MaxDepth, Headers.DepthUnit, Headers.HoursSoak, Headers.StartLatDeg, Headers.StartLatMin, Headers.StartLongDeg, Headers.StartLongMin, Headers.EndLatDeg, Headers.EndLatMin, Headers.EndLongDeg, Headers.EndLongMin, Headers.SetNum, LF.TrapNum, LF.TrapUsability, LF.Species, luUseCode.Description, Headers.StatArea, Headers.SubArea, LF.Sex, LF.WidthNotch, LF.GearCode, luGearCode.Description, LF.TrapType
# FROM (Headers INNER JOIN (LF INNER JOIN luUseCode ON (LF.TrapUsability = luUseCode.Code) AND (LF.TrapUsability = luUseCode.Code)) ON (Headers.Key = LF.HKey) AND (Headers.Key = LF.HKey)) INNER JOIN luGearCode ON (luGearCode.Code = LF.GearCode) AND (LF.GearCode = luGearCode.Code)
# WHERE (((Headers.Source)="GD" Or (Headers.Source)="GF" Or (Headers.Source)="GL" Or (Headers.Source)="GO" Or (Headers.Source)="GP" Or (Headers.Source)="GU" Or (Headers.Source)="GX" Or (Headers.Source)="GY"));
# 


## to do - dig into the differences in summarizing by set vs by location-day

# set up
setwd("D:/Documents//Projects/AIS/EGC/Crab catch data/")

library(lubridate)
library(sf)
library(gridExtra)
library(ggplot2)
library(tidyverse)
library(tidyr)
library(dplyr)

source("../EGC Models CSAS/ggplot-custom-theme.R")
geogr<-"EPSG:4269" # NAD 84


# Load HG and core data
hg<-read.csv("data_partners/HG/EGC_HG_formatted.csv")
coredat<-read.csv("data_partners/Core/EGC_core_formatted.csv")

# ---- DFO science crab bio ----

# crab bio database extraction - remove depletion trapping studies
bio<-read.csv("Database/Extractions/kg_all_crab_catch_2022-06-01.txt") %>% filter(Source !="GD")
bio <-bio %>% mutate(startLAT=StartLatDeg+(StartLatMin/60),
                     startLONG= (StartLongDeg+(StartLongMin/60))*-1,
                     endLAT=EndLatDeg+(EndLatMin/60),
                     endLONG=(EndLongDeg+(EndLongMin/60))*-1)  %>%
  dplyr::select(!c(StartLatDeg, StartLatMin, StartLongDeg, StartLongMin,EndLatDeg, EndLatMin, EndLongDeg, EndLongMin)) %>% 
  mutate(from="dfosci", 
         gear=case_when(GearCode==51~"fukui",GearCode==82~"snowcrab",GearCode==50~"minnow",GearCode==29~"beam_trawl",
                        GearCode==90~"beach_sample",GearCode==99~"unknown",GearCode==76~"phillips_research",GearCode=="A1"~"prawn",TRUE~"error")) 

table(bio$gear)

bio<-bio%>% dplyr::select(!c(luGearCode_Description,luUseCode_Description,GearCode,TrapType))


# # ----- load extra data from central coast 2021 that arent in the database yet ---- 
cc<-read.csv("Database/Data entered but not uploaded to CrabBio/GC_Catch_CentralCoast_2021-12-06.txt")
cc$from<-"dfosci_cc"
 
cc <-cc %>% mutate(startLAT=StartLatDeg+(StartLatMin/60),
                         startLONG= (StartLongDeg+(StartLongMin/60))*-1,
                         endLAT=EndLatDeg+(EndLatMin/60),
                         endLONG=(EndLongDeg+(EndLongMin/60))*-1)  %>%
 dplyr::select(!c(StartLatDeg, StartLatMin, StartLongDeg, StartLongMin,EndLatDeg, EndLatMin, EndLongDeg, EndLongMin)) %>% 
  mutate(gear="fukui",Key=NA)


# ----- combine bio data with the extra central coast data ----- 
dfosci<-rbind.data.frame(bio,cc %>% 
                           mutate(Sex=NA,WidthNotch=NA, MinDepth=NA,MaxDepth=NA,DepthUnit=NA) %>% 
                           dplyr::select(!Description)) %>% 
  dplyr::rename("year"="Year","day"="Day","month"="Month", "trapUsability"="TrapUsability", "area"="StatArea","subarea"="SubArea","geogLoc"="GeogLoc","duration_h"="HoursSoak","setNum"="SetNum","trapNum"="TrapNum", "source"="Source","species"="Species", "startLON"="startLONG","endLON"="endLONG")


# change blank species to empty trap
dfosci$species[dfosci$species==""]<-"848"

nrow(dfosci) #129725
 
# ------ correct names using lookup table (EGC_sites_to_replace.R) ------
names<-read.csv("../Crab catch data/site_renaming/greencrab_replace_site_byKey_2022-06-23.csv",stringsAsFactors = F)

dfosci<-merge(dfosci,names,by="Key", all.x=T, all.y=F)

dfosci$n<-1
dfosci %>% filter(is.na(site_name)) %>% select(GeogLoc,year)           %>% group_by(GeogLoc,year) %>% tally()
# !! a bunch of sites missing new name

dfosci %>% filter(is.na(site_name),year==2021) %>% select(geogLoc,day,setNum) %>% unique() %>% arrange(setNum)
# these are all from tammy's 2021 data

# rename central coast sites
dfosci<-dfosci %>% mutate(site_name=case_when((is.na(site_name)&year==2021& setNum==1)~"carterbay", #NEW SITE
                                              (is.na(site_name)&year==2021& setNum==2)~"carterbay",
                                              (is.na(site_name)&year==2021& setNum==3)~"carterbay",
                                              (is.na(site_name)&year==2021& setNum==4)~"griffinbooms", # NEW SITE
                                              (is.na(site_name)&year==2021& setNum==5)~"griffinnorth",# NEW SITE
                                              (is.na(site_name)&year==2021& setNum==6)~"windy",
                                              (is.na(site_name)&year==2021& setNum==7)~"bolin",
                                              (is.na(site_name)&year==2021& setNum==8)~"lohbrunnernorth",
                                              (is.na(site_name)&year==2021& setNum==9)~"pricebay",
                                              (is.na(site_name)&year==2021& setNum==10)~"higginswest",
                                              (is.na(site_name)&year==2021& setNum==11)~"higginswest",
                                              (is.na(site_name)&year==2021& setNum==12)~"lohbrunnernorth",
                                              (is.na(site_name)&year==2021& setNum==13)~"lohbrunnereast", #NEW SITE
                                              TRUE~site_name))
                                             
dfosci %>% dplyr::filter(geogLoc!=GeogLoc) # check if the old names match

dfosci<-dfosci %>% mutate(geogLoc=site_name) %>% select(!c(GeogLoc,site_name)) # replace old names with new site_name, and remove new fields

# add unique iD using new site name
dfosci$setID<-paste0("dfosci_",dfosci$year, "_",dfosci$month, "_",dfosci$day,"_", dfosci$setNum,"_",dfosci$geogLoc)

head(dfosci)

dfosci<-dfosci %>% relocate(setID,from) %>% select(!c(Key))


# ---- combine dfo core and sci data -----
dfo<-rbind.data.frame(dfosci %>% mutate(WidthPoint=NA) %>% select(!n), 
                      coredat %>% 
                        mutate(WidthNotch=NA, MinDepth=NA,MaxDepth=NA,DepthUnit=NA)) %>% 
                        mutate(gear=tolower(gear)) %>%   
                        dplyr::rename(latitude=startLAT, longitude=startLON)
nrow(dfo) #140690

write.csv(dfo,paste0("data_processed/allcrabs_EGCsurveys_byCrab_",Sys.Date(),".csv"),row.names=F)


# Look at list of species
allSpp<-dfo %>% group_by(species) %>% tally() %>% data.frame()
allSpp %>% filter(n<5) # uncommon species --- 015 = unkn fish, 4QD = Henricia, VIF = scaled crab Placetron, XKD = Cancer sp., XKI= pygmy rock crab, ZDD=Pugettia sp., ZDG = Pugettia richii

# recode sex 
dfo <-dfo %>% mutate(sex=case_when(Sex==1~"M",Sex==3~"F",Sex==4~"F_gravid",Sex==5~"F_spawned",Sex==0~"Unknown",is.na(Sex)~"Unknown",TRUE~"Unknown"))
table(dfo$sex)


# ---- subset only good traps of interest (i.e., fishing properly, fukui) and their set info
goodTraps<-dfo %>% filter(!(trapUsability %in% c(1,2,3,18)), source!="GD", gear=="fukui", duration_h != 0, !is.na(duration_h))
dfo %>% filter(is.na(duration_h))
nrow(goodTraps) #131429

# get cpue for all species in each set
nCrab_bySet1<-plyr::ddply(goodTraps,c("setID","setNum","from","species","sex"),summarize,total.caught=length(species))

# convert to matrix to fill in zeros for the species that were not caught in each set, then revert to long-mode
nCrab_bySet1<-reshape::cast(nCrab_bySet1,setID+sex~species)
nCrab_bySet1[is.na(nCrab_bySet1)]<-0
nCrab_bySet1<-reshape::melt(nCrab_bySet1,id.vars=c("setID","from","setNum"))

# make columns for numbers of each sex (reformat table back to matrix)
nCrab_bySet1<-reshape::cast(nCrab_bySet1,setID+species~sex)
nCrab_bySet1[is.na(nCrab_bySet1)]<-0 

# rename fields
nCrab_bySet1<-nCrab_bySet1 %>% dplyr::rename(n_F="F",n_F_gravid=F_gravid,n_F_spawned=F_spawned,n_M=M,n_Unk=Unknown)
names(nCrab_bySet1)
nCrab_bySet1$total.caught=rowSums(nCrab_bySet1[,c("n_F","n_F_gravid","n_F_spawned","n_M","n_Unk")])
head(nCrab_bySet1)

# ---- get set-level info for DFO data ---- 
set_info<-goodTraps %>% dplyr::group_by(setID,setNum,year,month,day,duration_h,from,geogLoc, latitude, longitude, area, subarea) %>% 
  summarize(nTraps = as.numeric(n_distinct(trapNum)))

# add site info to catch
nCrab_bySet<-merge(nCrab_bySet1, set_info, by="setID") 
nrow(nCrab_bySet) #6328

# now, REMOVE 848 (empty trap) since we have the tally for all other species
nCrab_bySet %>% filter(species==848)
nCrab_bySet<-nCrab_bySet %>% filter(!(species==848))

# there are comments in the core datasheet re: trap #s for haida gwaii - fix these manually
nCrab_bySet<-nCrab_bySet %>% mutate(nTraps=case_when(from!="db"&setNum==52& year==2020~18,
                                                          from!="db"&setNum==26& year==2021~20,
                                                     from!="db"&setNum==49& year==2020~6,
                                                     from!="db"&setNum==50& year==2020~6,
                                                     from!="db"&setNum==53& year==2020~12,
                                                     from!="db"&setNum==55& year==2020~6,
                                                     from!="db"&setNum==51& year==2020~6,
                                                     TRUE~nTraps))
nrow(nCrab_bySet) #61404


# ---- add HG to core and science data ---- 

# combine and add Cpue field
nCrab_bySet<-rbind.data.frame(nCrab_bySet %>% dplyr::select(!setNum), 
                              hg %>% dplyr::select(!c("gc.pa","date_start1","date_end1","sampled"))) %>%  
                              mutate(cpue=total.caught/(nTraps*duration_h))

nrow(nCrab_bySet) #61707
 
# ----- fix regions ----

# fix NAs in core data 
nCrab_bySet$area[nCrab_bySet$geogLoc=="San Juan 1"]<-20
nCrab_bySet$subarea[nCrab_bySet$geogLoc=="San Juan 1"]<-2

nCrab_bySet$area[nCrab_bySet$geogLoc=="Booth"]<-17
nCrab_bySet$subarea[nCrab_bySet$geogLoc=="Booth"]<-9

# assign regions and zones 
# (grouped regions, grouping Juan de Fuca with the rest of the Salish Sea, and grouping North Van Isl with the central coast) based on PFMAs
nCrab_bySet<- nCrab_bySet %>%
  mutate(., region = case_when(area %in% c(1,2) ~ "HG",
                               area %in% c(3:6) ~ "NC",
                               area %in% c(7:11) ~ "CC",
                               area %in% c(12) ~ "NVI",
                               area %in% c(13:19, 28,29)  ~ "SS",
                               area == 20 ~ "JDF",
                               area %in% c(21:27) ~ "WCVI")) %>%
  mutate(., zone = case_when(area %in% c(1,2) ~ "HG",
                             area %in% c(3:6) ~ "NC",
                             area %in% c(7:12) ~ "CC",
                             area %in% c(13:20, 28,29)  ~ "SS",
                             area %in% c(21:27) ~ "WCVI"))%>%
  mutate(., region.name=case_when(region=="HG" ~ "Haida Gwaii",region=="JDF" ~ "Juan de Fuca",region=="SS" ~ "Salish Sea",region=="WCVI" ~ "W. Coast Van. Isl.",region %in% c("NC") ~ "North Coast", region %in% c("CC") ~ "Central Coast", region %in% c("NVI") ~ "North Van. Isl."))

nCrab_bySet<-nCrab_bySet  %>% 
  mutate(region=case_when(from=="HG"~"HG", TRUE~region))%>% 
  mutate(zone=case_when(from=="HG"~"HG", TRUE~zone)) %>% 
  mutate(region.name=case_when(from=="HG"~"Haida Gwaii", TRUE~region.name))

# ----- output -----
head(nCrab_bySet)
nrow(nCrab_bySet) #61707
nrow(nCrab_bySet %>% filter(species=="XMB")) #3227

dat<-nCrab_bySet
sum(dat$total.caught[dat$species=="XMB"]) #68067


# ---- write data by SET ----- 
dat<-dat %>% dplyr::relocate("setID", "from", "year", "month", "latitude", "longitude", "area", "subarea","geogLoc", "region", "zone", "region.name", "nTraps", "duration_h", "species", "n_F", "n_F_gravid", "n_F_spawned", "n_M", "n_Unk", "total.caught", "cpue") %>% arrange(year,month)

# write data by SET
write.csv(dat,paste0("data_processed/allcrabs_EGCsurveys_fukui_noGD_bySet_",Sys.Date(),".csv"), row.names=F)
write.csv(dat %>% filter(species=="XMB") ,paste0("data_processed/EGC_EGCsurveys_fukui_noGD_bySet_",Sys.Date(),".csv"), row.names=F)

# read as spatial file to write .shp with proper field formatting
s<-st_read(paste0("data_processed/allcrabs_EGCsurveys_fukui_noGD_bySet_",Sys.Date(),".csv"), options=c("X_POSSIBLE_NAMES=longitude", "Y_POSSIBLE_NAMES=latitude"))
s<-st_set_crs(s,geogr)

names(s)[names(s)=="total.caught"]<-"nCrabs"
s<-s %>% mutate(cpue=as.numeric(cpue),nCrabs=as.numeric(nCrabs),year=as.numeric(year),duration_h=as.numeric(duration_h), nTraps=as.numeric(nTraps))

names(s)<-strtrim(names(s),10)

st_write(s %>% filter(!is.na(latitude)),dsn="data_processed/shp",layer=paste0("allcrabs_EGCsurveys_fukui_noGD_bySet_",Sys.Date()), driver="ESRI Shapefile",append=FALSE)
st_write(s %>% filter(!is.na(latitude), species=="XMB"),dsn="data_processed/shp",layer=paste0("EGC_EGCsurveys_fukui_noGD_bySet_",Sys.Date()), driver="ESRI Shapefile",append=FALSE)

# -------- summarize by site ------- 

# get monthly summary for unique fishing events
set_info_all<-dat %>% select(setID,year,month,day,duration_h,from,geogLoc, area,subarea,latitude, longitude, nTraps) %>% unique()
nrow(set_info_all) #3227

site.coords<-set_info_all %>% group_by(geogLoc,from) %>% 
  summarize(lat_site_mean=mean(latitude, na.rm=T),lon_site_mean=mean(longitude, na.rm=T))

site.effort.month<-set_info_all %>% group_by(year,month,geogLoc,from) %>% 
  summarize(total_nTraps=sum(nTraps), total_duration_h=round(sum(duration_h),2), trap.hr=total_nTraps*total_duration_h)

site.effort.month<-merge(site.effort.month, site.coords, by=c("geogLoc","from"),all.x=T,all.y=T)

nrow(site.effort.month) #1797

# get monthly crab catches by site
site.catch.month<-dat %>% group_by(species,year,month,from,geogLoc) %>% summarize(nInd=sum(total.caught),nSets=n_distinct(setID))
nrow(site.catch.month) #35657

# combine catch and effort
site<-merge(site.catch.month, site.effort.month, by=c("year","month","from","geogLoc"),all.x=T,all.y=T)
nrow(site) #35657

# calculate monthly CPUE
site<-site %>% mutate(cpue.month=nInd/trap.hr) %>% 
  select(!trap.hr) %>% relocate(from,year,month,geogLoc,lat_site_mean,lon_site_mean,species) %>% 
  arrange(year,month,from,geogLoc)

head(site)
site <-site %>% filter(geogLoc!="possibleGPSerror") # remove bad coords, they all get averaged together so not useful.


write.csv(site,paste0("data_processed/allcrabs_EGCsurveys_fukui_noGD_bySite_",Sys.Date(),".csv"),row.names=F)
write.csv(site %>% filter(species=="XMB"),paste0("data_processed/EGC_EGCsurveys_fukui_noGD_bySite_",Sys.Date(),".csv"),row.names=F)


# read as spatial file to write .shp with proper field formatting
s1<-st_read(paste0("data_processed/allcrabs_EGCsurveys_fukui_noGD_bySite_",Sys.Date(),".csv"), options=c("X_POSSIBLE_NAMES=lon_site_mean", "Y_POSSIBLE_NAMES=lat_site_mean"))
s1<-st_set_crs(s1,geogr)

names(s1)[names(s1)=="total_nTraps"]<-"nTraps"
names(s1)[names(s1)=="total_duration_h"]<-"sum_soak_h"
s1<-s1 %>% mutate(cpue.month=as.numeric(cpue.month),nInd=as.numeric(nInd),year=as.numeric(year),duration_h=as.numeric(sum_soak_h), nTraps=as.numeric(nTraps))

names(s1)<-strtrim(names(s1),10)

st_write(s1 %>% filter(!is.na(lat_site_m), species=="XMB"),dsn="data_processed/shp",layer=paste0("EGC_EGCsurveys_fukui_noGD_bySite_",Sys.Date()), driver="ESRI Shapefile",append=FALSE)




